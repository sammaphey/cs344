from random import shuffle
from math import floor, ceil

def iterative_search(array):
    mins = []
    maxs = []

    half = ceil(len(array) / 2)

    for i in range(0, len(array), 2):
        if array[i] < array[i+1]:
            # tmin = array[i]
            # tmax = array[i+1]
            mins.append(array[i])
            maxs.append(array[i+1])
        else:
            # tmin = array[i+1]
            # tmax = array[i]
            mins.append(array[i+1])
            maxs.append(array[i])

        for i in range(0, half):
            if mins[i] < mins[i+1]:
                


    print(mins, maxs)

    return 0, 0, 0

def recursive_search(array, cost):
    if len(array) == 1:
        minimum = array[0]
        maximum = array[0]

        return minimum, maximum, cost

    if len(array) == 2:
        cost += 1
        if array[0] < array[1]:
            minimum = array[0]
            maximum = array[1]
        else:
            minimum = array[1]
            maximum = array[0]

        return minimum, maximum, cost

    else:
        cost += 1
        r = array[floor(len(array) / 2):]
        l = array[:ceil(len(array) / 2)]

        min_l, max_l, cost = recursive_search(l, cost)
        min_r, max_r, cost = recursive_search(r, cost)

        if min_l < min_r:
            minimum = min_l
        else:
            minimum = min_r

        if max_l > max_r:
            maximum = max_l
        else:
            maximum = max_r

        return minimum, maximum, cost


if __name__ == "__main__":

    try:
        size = int(input("enter array size: "))
        outfile = "out.txt"

    except KeyboardInterrupt:
        print("\nBye Bye")

    # for length in range(2, size + 1):
    #     array = [i for i in range(length)]
    #     shuffle(array)
    #     cost = 0
    #     minimum_r, maximum_r, cost_r = recursive_search(array, cost)
    #     assert minimum_r == 0
    #     assert maximum_r == length-1
    #     minimum_i, maximum_i, cost_i = iterative_search(array)
    #     assert minimum_i == 0
    #     assert maximum_i == length-1
    #
    #     with open("out.txt", "a") as f:
    #         f.write(str(length) + " " + str(cost_r) + " " + str(cost_i) + "\n")
    array = [i for i in range(size)]
    shuffle(array)
    minimum_i, maximum_i, cost_i = iterative_search(array)
