#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <assert.h>

using namespace std;

void show( int[], int );
void shuffle( int[], int );
void swap( int&, int& );

// show array
void show( int array[], int size )
{
cerr << "[";
for (int i=0; i<size; i++) {
  cerr << array[i] << " ";
}
cerr << "]" << endl;
}

// shuffle array
void shuffle( int array[], int size )
{
srand( time(0) );
int index;
for (int i=size-1; i>0; i--) {
  index = rand() % size;
  swap( array[index], array[i] );
}
}

// swap two integers
void swap( int& a, int& b )
{
int tmp;
tmp = a;
a = b;
b = tmp;
}
