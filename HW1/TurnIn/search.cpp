#include <stdio.h>      /* printf */
#include <math.h>       /* floor */
#include <iostream>
#include <stack>

using namespace std;

void iterative_search(int array[], int size, int& min, int& max, int& cost) {

      if(array[0] < array[1]) {
        cost++;
        min = array[0];
        max = array[1];
      } else {
        cost++;
        min = array[1];
        max = array[0];
      }

      for(int i = 2; i < size; i+=2) {
        int tmin, tmax;
        cost++;
        if(array[i] < array[i+1]) {
          tmin = array[i];
          tmax = array[i+1];
        } else {
          tmin = array[i+1];
          tmax = array[i];
        }

        cost++;
        if(tmin < min) {
          min = tmin;
        }

        cost++;
        if(tmax > max) {
          max = tmax;
        }
      }
};

void recursive_search(int array[], int size, int& min, int& max, int& cost) {

    if(size == 1) {
      min = array[0];
      max = array[0];
    }
    else if(size == 2) {
      cost++;
      if(array[0] < array[1]) {
        min = array[0];
        max = array[1];
      } else {
        min = array[1];
        max = array[0];
      }
    } else {
      int ls, rs, min_l, min_r, max_l, max_r;

      ls = floor( double(size) / 2 );
      rs = ceil( double(size) / 2 );

      int l[ls], r[rs];

      for (int i = 0; i < ls; i++) {
        l[i] = array[i];
      }

      for (int j = 0; j < rs; j++) {
        r[j] = array[j+ls];
      }

      recursive_search(l, ls, min_l, max_l, cost);
      recursive_search(r, rs, min_r, max_r, cost);

      cost++;
      if(min_l < min_r) {
        min = min_l;
      } else {
        min = min_r;
      }

      cost++;
      if(max_l > max_r) {
        max = max_l;
      } else {
        max = max_r;
      }

    }
}
