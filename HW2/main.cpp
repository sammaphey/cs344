#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <vector>

using namespace std;

// swap with no temp
#define swap(a,b)	((a)^=(b), (b)^=(a), (a)^=(b))

// prototypes of sorts
void bsort(int array[], int size);
void isort(int array[], int size);
void ssort(int array[], int size);
void msort(int array[], int size);
void my_qsort(int array[], int size, int (*choose_pivot)(int [], int));

// median finder
void select(int array[], int size, int K, int &pivot);

// helpers
void show(int array[], int size);
void copy(int src[], int dest[], int size);
void shuffle(int array[], int size);
void is_sorted(int array[], int size);

// pivot choosers
int random_pivot(int [], int);
int fixed_pivot(int [], int);
int median_pivot(int [], int);

// global cost variable
int cost;


///////////////////////////////////////////////////////////////////////
// main: testing all sorts
///////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
	assert(argc > 1);

	// get size
	int total_size = atoi(argv[1]);
	ofstream outfile;
	cerr << "Size = " << total_size << endl;
	cerr << "Filename = " << argv[2] << endl;

	outfile.open( argv[2] );

	for (int size = 1; size < total_size; size++) {
		// allocate array
		int test_array[size];
		for (int i=0; i<size; i++) {
			test_array[i] = i;
		}
		// shuffle(test_array, size); 

		int array[size];

		outfile << size << " ";

		// testing mergesort
		// cost = 0;
		// copy(test_array, array, size);
		// msort(array, size);
		// is_sorted(array, size);
		// outfile << cost << " ";

		// testing quicksort (with random pivot)
		// cost = 0;
		// copy(test_array, array, size);
		// my_qsort(array, size, random_pivot);
		// is_sorted(array, size);
		// outfile << cost << " ";

		// testing quicksort (with median pivot)
		cost = 0;
		copy(test_array, array, size);
		my_qsort(array, size, median_pivot);
		is_sorted(array, size);
		outfile << cost << " ";

		// testing insertion sort
		// cost = 0;
		// copy(test_array, array, size);
		// isort(array, size);
		// is_sorted(array, size);
		// outfile << cost << " ";

		// testing selection sort
		// cost = 0;
		// copy(test_array, array, size);
		// ssort(array, size);
		// is_sorted(array, size);
		// outfile << cost << " ";

		// testing bubble sort
		// cost = 0;
		// copy(test_array, array, size);
		// bsort(array, size);
		// is_sorted(array, size);
		// outfile << cost << " ";

		outfile << endl;
	}
}

///////////////////////////////////////////////////////////////////////
// HELPERS
///////////////////////////////////////////////////////////////////////

// randomly shuffle array
void shuffle(int array[], int size) {
	int index;
	srand(time(0));
	for (int i=size-1; i>0; i--) {
		index = rand() % i;
		swap(array[i], array[index]);
	}
}

// display array
void show(int array[], int size) {
	cerr << "[";
	for (int i=0; i<size; i++) {
		cerr << array[i] << " ";
	}
 cerr << "]";
	cerr << endl;
}

// array is sorted
void is_sorted(int array[], int size) {
	for (int i=0; i<size-1; i++) {
		assert(array[i] <= array[i+1]);
	}
}

// copy array
void copy(int src[], int dest[], int size) {
	for (int i=0; i<size; i++) {
		dest[i] = src[i];
	}
}

// pivot choosers
int fixed_pivot(int array[], int size) {
	return array[0];
}

int random_pivot(int array[], int size) {
	return array[rand()%size];
}

int median_pivot(int array[], int size) {
	int pivot = 0;
	select(array, size, size>>1, pivot);
	return pivot;
}

void select(int array[], int size, int K, int &pivot) {

	//base case
	if(size<5){
		assert(0 <= K && K <5);
		bsort(array,size);
		pivot = array[K];
		return;
	}
	for(int i =0; i+5<=size; i=i+5){
		bsort(array+i,5);
	}
	int num_medians = size/5;
	int medians[num_medians];

for(int i=0;i<num_medians;i++){
medians[i] = array[5*i+2];
}
//recurse to find median of medians
select(medians, num_medians, num_medians/2, pivot);
//find the index of the fixed_pivot
int index =0;
for(int i=0;i<size;i++){
	cost++;
	if(array[i] == pivot){
		index = i;
		break;
	}
}
//set pivot aside
swap(array[index],array[size-1]);
vector<int> left_vec, right_vec;
//partition around pivot
for(int i=0; i<size-1;i++){
cost++;
if(array[i] <= pivot){
	left_vec.push_back(array[i]);
}
	else{
		right_vec.push_back(array[i]);
	}
}

int left_size = left_vec.size();
int right_size = right_vec.size();

//prepare to recurse if necessary
if(K < left_size){
	//prepare left subarray
	int left_array[left_size];
	for(int i=0;i<left_size;i++){
		left_array[i] = left_vec[i];
	}
}
else if(K == left_size){
	pivot = pivot;
}
else{
	//prepare right subarray
	int right_array[right_size];
	for(int i=0; i<right_size; i++){
		right_array[i] = right_vec[i];
	}
	select(right_array, right_size, K-(left_size+1), pivot);
}

	return;
}
