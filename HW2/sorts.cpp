#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <math.h>       /* floor */

using namespace std;

void bsort(int[], int); // bubble sort
void isort(int[], int); // insertion sort
void ssort(int[], int); // selection sort
void msort(int[], int); // mergesort
void my_qsort(int[], int, int (*)(int [], int)); // quicksort// median finder
void select(int array[], int size, int K, int &answer);
void show( int[], int );
void shuffle( int[], int );
void quicksort(int[], int, int);
int partition(int[], int, int);

extern int cost;


void bsort(int array[], int size) {

  for( int i = 0; i < size-1; i++) {
    for( int j = 0; j < size-i-1; j++) {
      int temp;
      if(array[j] > array[j+1]) {
        cost++;
        temp = array[j];
        array[j] = array[j+1];
        array[j+1] = temp;
      }
    }
  }
}

void isort(int array[], int size) {
  int last, j;

  last = array[size-1];

  if(size > 1) {

    isort(array, size-1);
    j = size-2;

    while (j >= 0 && array[j] > last) {
      cost++;
      array[j+1] = array[j];
      j--;
    }
    array[j+1] = last;
  }

}

void ssort(int array[], int size) {
  int max = 0, temp = 0;
  for (int i = 0; i < size; i++) {
    if (array[i] > array[max]) {
      cost++;
      max = i;
    }
  }
  temp = array[size-1];
  array[size-1] = array[max];
  array[max] = temp;

  if (size > 1) {
    ssort(array, --size);
  }
}

void merge(int array[], int l_array[], int r_array[], int size, int l, int r) {
	int i = 0, j = 0;

  cost++;

	for(int k = 0; k < size; k++) {
		if (i==l){
			array[k]=r_array[j];
			j++;
		} else if(j==r){
			array[k]=l_array[i];
			i++;
		}else if(l_array[i] < r_array[j] ) {
			array[k] = l_array[i];
			i++;
		} else {
			array[k] = r_array[j];
			j++;
		}
	}

}

void msort(int array[], int size) {
	int l, r, mid;

	if(size == 2) {
		cost++;
		if(array[0] > array[1]) {
			swap(array[0], array[1]);
		}
		return;
	}
	if(size == 1) {
		return;
	}

  cost++;

  mid = floor( double(size) / 2 );

	l = mid;
	r = size - mid;

	int l_array[l], r_array[r];

	for(int i = 0; i < l; i++) {
		l_array[i] = array[i];
	}

	for(int i = mid; i < size; i++) {
		r_array[i-mid] = array[i];
	}

	msort(l_array, l);
	msort(r_array, r);

	merge(array, l_array, r_array, size, l, r);

}

void my_qsort(int array[], int size, int (*choose_pivot)(int [], int)) {

if(size <=1)
return;

if(size<=2){
  cost++;
  if(array[0] > array[1]){
    swap(array[0],array[1]);
  }
  return;
}

  int pivot = (*choose_pivot)(array, size);

  for(int i = 0; i < size; i++) {
    if(array[i] == pivot) {
      swap(array[i], array[size-1]);
    }
  }

  quicksort(array, 0, size-1);

}

int partition(int array[], int low, int high) {
  int pivot = array[high];

  int i = low - 1;


  for(int j = low; j <= high - 1; j++) {
    cost++;
    if(array[j] <= pivot) {
      i++;
      swap(array[i], array[j]);
    }
  }

  swap(array[i + 1], array[high]);
  return (i + 1);
}

void quicksort(int array[], int low, int high) {
  // cost++;
  if( low < high) {
    int pivot = partition(array, low, high);

    quicksort(array, low, pivot - 1);
    quicksort(array, pivot + 1, high);
  }
}
