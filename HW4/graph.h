#ifndef GRAPH_H
#define GRAPH_H

#include <sstream>
#include <vector>
#include <map>
using namespace std;

template <class T>
class Graph 
{
private:
	typedef typename std::map< T, vector<T> > AdjacencyList;
	AdjacencyList adjacency_list;
	int size;
	bool directed;

public:
	// constructors
	Graph():size(0), directed(false) { adjacency_list.clear(); }
		// for empty graph
	Graph( const vector<T> &vertex_set, bool is_directed );
		// for random graph
	Graph( const Graph<T> &g ):size(g.size), directed(g.directed)
		{ adjacency_list = g.adjacency_list; }
		// copy constructor

	// destructor
	~Graph() { adjacency_list.clear(); }

	// accessors
	int getSize() { return size; }
		// returns the number of vertices
	vector<T> vertices();	
		// returns all vertices in the graph

	// predicates
	bool is_vertex( const T &u ) const;				
		// true if u is a vertex in the graph
	bool is_edge( const T &u, const T &v ) const;	
		// true if (u,v) is an edge in the graph
	bool is_directed() const { return directed; }
		// true if the graph is directed

	// mutator(s)
	void setDirected(bool b) { directed = b; }
	void insert( const T &u, const T &v );
		// adds a new edge (u,v) to the graph


	// iterator for vertices
	//  reuses iterator for std::map
	typedef typename AdjacencyList::const_iterator vertex_iterator;
	vertex_iterator begin() const { return adjacency_list.begin(); }
	vertex_iterator end() const { return adjacency_list.end(); }


	// iterator for neighborhoods
	//  uses an inner iterator class
	class neighbor_iterator {
	public:
		neighbor_iterator( Graph<T> &g, T &v ) 
			{ alist = g.adjacency_list[v]; }
		T begin() 	{ it = alist.begin(); return *it; }
		bool end() 	{ return (it == alist.end()); }
		T next() 	{ return *(++it); }
	private:
		typedef typename vector<T>::const_iterator private_iterator;
		vector<T> alist;
		private_iterator it;
	};


	// overloaded output (generates graphviz format)
	friend ostream & operator<<(ostream &out, Graph<T> &graph) {
		if(graph.is_directed()){
			out << "digraph {" << endl;
		} else {
			out << "graph {" << endl;
		}
		
		for(vertex_iterator i = graph.begin(); i != graph.end(); i++){
			T v = i->first;
			neighbor_iterator itr(graph, v);
			if(graph.is_directed()){
				for(T u = itr.begin(); !itr.end(); u = itr.next()){
					out << '\t' << v << " -> " << u << ';' << endl;
				}
			}

			if(!graph.is_directed()){
				for(T u = itr.begin(); !itr.end(); u = itr.next()){
					out << '\t' << v << " -- " << u << ';' << endl;
				}
			}
			out << endl;
		}
		out << "}" << endl;
		
		return out;
	}

	// overloaded input (assumes graphviz format)
	friend istream & operator>>(istream &in, Graph<T> &graph) {
		while(!in.eof()){
			string curr_line;
			getline(in, curr_line);

			if(curr_line.compare("digraph {") == 0 || curr_line.compare("graph {") == 0){
				continue;
			}
			
			if(in.eof()){
				return in;
			}

			istringstream local_stream(curr_line);

			T vertex_value, neighbor_value;

			char c;
			//do while loop of eliminate space ,tab, -, >
			do{c = local_stream.get();}
			while(c == ' ' || c == '\t' || c == '-' || c == '>');
			local_stream.unget();

			local_stream >> vertex_value;
			
			//cout << vertex_value << endl;
			//do while again
			do{c = local_stream.get( );}
			while(c == ' ' || c == '\t' || c == '-' || c == '>');
			local_stream.unget();

			while(local_stream >> neighbor_value){
				//cout << "io" << neighbor_value << endl ;
				graph.insert(vertex_value, neighbor_value);
			}
		}
		return in;
	}


	// graph searches
	Graph<T> BFS( const T &start_vertex );
	Graph<T> DFS();

};


#include "graph.cpp"

#endif


