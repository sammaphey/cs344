#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <queue>
#include <stack>
#include <assert.h>
#include <time.h>
using namespace std;


/////////////////////////////////////////////////////////
// constructors
/////////////////////////////////////////////////////////

// construct a random graph
template <class T>
Graph<T>::Graph( const vector<T> &vertex_set, bool is_directed )
{
	size = vertex_set.size();
	directed = is_directed;
	
	AdjacencyList list;
	bool in;
	for(int i = 0; i < size; i++){
		T v = vertex_set.at(i);
		for(int j = 0; j <size; j++){
			if(i != j){
				in = (rand() % 2);
				if(in){
					list[v].push_back(vertex_set.at(j));
				}
			}
		}
	}

	adjacency_list = list;
}

/////////////////////////////////////////////////////////
// accessors
/////////////////////////////////////////////////////////

template <class T>
vector<T> Graph<T>::vertices()
{
	vector<T> vertex_set;

	for (vertex_iterator i = begin(); i != end(); i++) {
		vertex_set.push_back(i->first);
	}
	return vertex_set;
}



/////////////////////////////////////////////////////////
// mutators
/////////////////////////////////////////////////////////

// insert edge
template <class T>
void Graph<T>::insert( const T &u, const T &v )
{
	adjacency_list[u].push_back(v);
}



/////////////////////////////////////////////////////////
// predicates
/////////////////////////////////////////////////////////

template <class T>
bool Graph<T>::is_vertex( const T &u ) const
{
	return (adjacency_list.find(u) != end());
}

template <class T>
bool Graph<T>::is_edge( const T &u, const T &v ) const
{
	vertex_iterator i = adjacency_list.find(u);
	if (i == end()) {
		return false;
	}
	assert( i->first == u );

	typedef typename std::vector<T> NeighborList;
	typedef typename NeighborList::const_iterator neighbor_iterator;

	NeighborList neighbors = i->second;
	neighbor_iterator j = find( neighbors.begin(), neighbors.end(), v );
	return (j != neighbors.end()) ? true : false;
}





////////////////////////////////////////////////////////////
// Breadth First Search
////////////////////////////////////////////////////////////
#define INFINITY	-1
enum COLOR {WHITE, GRAY, BLACK};

template <class T>
struct BFS_Vertex {
	COLOR color;
	int distance;
	T previous;
};


template <class T>
Graph<T> Graph<T>::BFS( const T & start_vertex )
{
	map<T, BFS_Vertex<T> > BFS_Tree;
	Graph<T> outputTree;
	queue<T> BFS_Queue;

	for(vertex_iterator i = begin(); i != end(); i++) {
		T current = i ->first;

		if(current != start_vertex) {
			BFS_Tree[current].color = WHITE;
			BFS_Tree[current].distance = INFINITY;
			BFS_Tree[current].previous = 0;
		}
	}

	BFS_Tree[start_vertex].color = GRAY;
	BFS_Tree[start_vertex].distance = 0;
	BFS_Tree[start_vertex].previous = 0;

	BFS_Queue.push(start_vertex);

	while(!BFS_Queue.empty()) {
		T elem = BFS_Queue.front();
		BFS_Queue.pop();

		vector<T> next_verts = adjacency_list[elem];
		for(typename vector<T>::size_type i = 0; i < next_verts.size(); i++) {
			T vertex = next_verts[i];

			if(BFS_Tree[vertex].color == WHITE) {
				BFS_Tree[vertex].color = GRAY;
				BFS_Tree[vertex].distance = BFS_Tree[elem].distance + 1;
				BFS_Tree[vertex].previous = elem;
				BFS_Queue.push(vertex);
				outputTree.insert(elem, vertex);
			}
		}

		BFS_Tree[elem].color = BLACK;

	}

	/*for(vertex_iterator i = begin(); i != end(); i++){
		T current = i ->first;

		if(BFS_Tree[current].color == WHITE){
			BFS_Tree[current].color = GRAY;
			BFS_Tree[current].distance = 0;
			BFS_Tree[current].previous = 0;
			BFS_Queue.push(current);

			while(!BFS_Queue.empty()) {
				T elem = BFS_Queue.front();
				BFS_Queue.pop();

				vector<T> next_verts = adjacency_list[elem];
				for(typename vector<T>::size_type i = 0; i < next_verts.size(); i++){
					T vertex = next_verts[i];

					if(BFS_Tree[vertex].color == WHITE) {
						BFS_Tree[vertex].color = GRAY;
						BFS_Tree[vertex].distance = BFS_Tree[elem].distance + 1;
						BFS_Tree[vertex].previous = elem;
						BFS_Queue.push(vertex);
						outputTree.insert(elem, vertex);
					}
				}
				BFS_Tree[elem].color = BLACK;
			}
		}
	}*/

	return outputTree;
}




////////////////////////////////////////////////////////////
// Depth First Search
////////////////////////////////////////////////////////////
template <class T>
struct DFS_Vertex {
	COLOR color;
	int discover_time, finish_time;
	T previous;
};


template <class T>
Graph<T> Graph<T>::DFS()
{
	map<T, DFS_Vertex<T> > DFS_Tree;
	Graph<T> outputTree;
	stack<T> DFS_Stack;

	for(vertex_iterator i = begin(); i != end(); i++)  {
		T current = i ->first;
		DFS_Tree[current].color = WHITE;
		DFS_Tree[current].previous = 0;
		DFS_Tree[current].discover_time = INFINITY;
		DFS_Tree[current].finish_time = INFINITY;
	}

	int _time = 0;

	for(vertex_iterator i = begin(); i != end(); i++)  {
		T current = i ->first;

		if(DFS_Tree[current].color == WHITE) {
			
			DFS_Stack.push(current);
			_time++;
			DFS_Tree[current].discover_time = _time;
			DFS_Tree[current].color = GRAY;

			while(!DFS_Stack.empty()) {
				T elem = DFS_Stack.top();
				vector<T> next_verts = adjacency_list[elem];

				bool unexploredNext = false;
				for(typename vector<T>::size_type i = 0; i < next_verts.size(); i++) {
					T vertex = next_verts[i];

					if(DFS_Tree[vertex].color == WHITE) {
						DFS_Tree[vertex].previous = elem;

						outputTree.insert(elem, vertex);

						DFS_Stack.push(vertex);
						_time++;
						DFS_Tree[vertex].discover_time = _time;
						DFS_Tree[vertex].color = GRAY;

						unexploredNext = true;
						break;
					}
				}
				//if explored nieghbor pop and finish
				if(!unexploredNext){
					DFS_Tree[elem].color = BLACK;
					_time++;
					DFS_Tree[elem].finish_time = _time;
					DFS_Stack.pop();
				}
			}
		}
	}

	return outputTree;
}
