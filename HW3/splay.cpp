template <class T>
Splay<T> * Splay<T>::search( const T &v ) {
  typedef Tree<T> Base;

  if(v == Base::value) {
    return this;
  } else if (v < Base::value) {
    if(Base::left != NULL) {
      return (Splay<T> *)Base::left->search(v);
    } else {
      return NULL;
    }
  } else {
    if(Base::right != NULL) {
      return (Splay<T> *)Base::right->search(v);
    } else {
      return NULL;
    }
  }

  splay();
}

template <class T>
Splay<T> * Splay<T>::insert( const T &v ) {
  typedef Tree<T> Base;

  if(Base::value < v) {
    if(Base::right == NULL) {
      Base::right = new Splay<T>(v, 0, 0, this);
    } else {
      Base::right = Base::right->insert(v);
    }
  } else {
    if(Base::left == NULL) {
      Base::left = new Splay<T>(v, 0, 0, this);
    } else {
      Base::left = Base::left->insert(v);
    }
  }

  return splay_insert(v);
}

template <class T>
Splay<T> * Splay<T>::remove( const T &v ) {
  typedef Tree<T> Base;

  Splay<T> * temp;

  if(v == Base::value) {

    if(Base::left == NULL && Base::right == NULL) {
      delete this;
    } else if(Base::left == NULL && Base::right != NULL ) {
      temp = (Splay<T> *)Base::right;
      temp->setParent(Base::parent);
      delete this;
      return temp;
    } else if(Base::left != NULL && Base::right == NULL) {
      temp = (Splay<T> *)Base::left;
      temp->setParent(Base::parent);
      delete this;
      return temp;
    } else {
      temp = (Splay<T> *)Base::successor();
      T new_temp = temp->value;

      Base::right = (Splay<T> *)Base::right->remove(new_temp);
      Base::value = new_temp;

      return splay();
    }

  } else if(v > Base::value) {
    Base::right = Base::right->remove(v);
    return splay();
  } else {
    Base::left = Base::left->remove(v);
    return splay();
  }

  return NULL;

}

template <class T>
Splay<T> * Splay<T>::splay( ) {
  typedef Tree<T> Base;
  Splay<T> * root;
  Splay<T> * daddy;
  Splay<T> * granddaddy;

  root = getRoot();
  daddy = (Splay<T> *)Base::parent;

  if(daddy == NULL) {
    granddaddy = NULL;
  } else {
    granddaddy = (Splay<T> *)daddy->getParent();
  }

  if(Base::parent == NULL) {
    return this;
  } else {
    if(granddaddy == NULL) {
      if(Base::value < root->getValue()) {
        // right rotate
        Tree <T> * T2 = this->getRight();

        daddy->setLeft(T2);

        if(T2 != NULL) {
          T2->setParent(daddy);
        }

        this->setRight(daddy);
        this->setParent(NULL);
        daddy->setParent(this);

        return this;
      } else {
        // left rotate
        Tree <T> * T2 = this->getLeft();

        daddy->setRight(T2);

        if(T2 != NULL) {
          T2->setParent(daddy);
        }

        this->setLeft(daddy);
        this->setParent(NULL);
        daddy->setParent(this);

        return this;
      }
    } else {
      if(Base::value < daddy->getValue()) {
        if(daddy->getValue() < granddaddy->getValue()) {
          // right right
          Splay<T> * T1 = (Splay<T> *)this->getLeft();
          Splay<T> * T2 = (Splay<T> *)this->getRight();
          Splay<T> * T3 = (Splay<T> *)daddy->getRight();
          Splay<T> * T4 = (Splay<T> *)granddaddy->getRight();

          T temp = granddaddy->getValue();
          granddaddy->setValue(this->getValue());
          granddaddy->setLeft(T1);

          if(T1 != NULL)
            T1->setParent(granddaddy);

          granddaddy->setRight(daddy);
          daddy->setParent(granddaddy);

          daddy->setLeft(T2);

          if(T2 != NULL)
            T2->setParent(daddy);

          daddy->setRight(this);
          this->setValue(temp);

          this->setLeft(T3);
          if(T3 != NULL)
            T3->setParent(this);

          this->setRight(T4);
          if(T4 != NULL)
            T4->setParent(this);

          return (Splay<T> *)granddaddy->getParent();
        } else {
          // left right
          Splay<T> * T1 = (Splay<T> *)granddaddy->getLeft();
          Splay<T> * T2 = (Splay<T> *)this->getLeft();
          Splay<T> * T3 = (Splay<T> *)this->getRight();
          Splay<T> * T4 = (Splay<T> *)daddy->getRight();

          int temp = granddaddy->getValue();
          granddaddy->setValue(this->getValue());
          granddaddy->setLeft(T1);

          if(T1 != NULL)
            T1->setParent(granddaddy);

          granddaddy->setRight(T2);

          if(T2 != NULL)
            T2->setParent(granddaddy);

          daddy->setLeft(T3);

          if(T3 != NULL)
            T3->setParent(daddy);

          daddy->setRight(T4);

          if(T4 != NULL)
            T4->setParent(daddy);

          this->setValue(temp);

          this->setLeft(granddaddy);
          this->setRight(daddy);

          return (Splay<T> *)granddaddy->getParent();
        }
      } else {
        if(daddy->getValue() > granddaddy->getValue()) {
          // left left
          Splay<T> * T1 = (Splay<T> *)granddaddy->getLeft();
          Splay<T> * T2 = (Splay<T> *)daddy->getLeft();
          Splay<T> * T3 = (Splay<T> *)this->getLeft();
          Splay<T> * T4 = (Splay<T> *)this->getRight();

          T temp = granddaddy->getValue();
          granddaddy->setValue(this->getValue());
          granddaddy->setLeft(T1);

          if(T1 != NULL)
            T1->setParent(granddaddy);

          granddaddy->setRight(T2);

          if(T2 != NULL)
            T2->setParent(granddaddy);

          daddy->setRight(T3);

          if(T3 != NULL)
            T3->setParent(daddy);

          daddy->setLeft(this);
          this->setParent(daddy);

          this->setValue(temp);

          granddaddy->setRight(T4);

          if(T4 != NULL)
            T4->setParent(granddaddy);

          granddaddy->setLeft(daddy);

          return (Splay<T> *)granddaddy->getParent();
        } else {
          // right left
          Splay<T> * T1 = (Splay<T> *)daddy->getLeft();
          Splay<T> * T2 = (Splay<T> *)this->getLeft();
          Splay<T> * T3 = (Splay<T> *)this->getRight();
          Splay<T> * T4 = (Splay<T> *)granddaddy->getRight();

          T temp = granddaddy->getValue();
          granddaddy->setValue(this->getValue());
          granddaddy->setLeft(T3);

          if(T3 != NULL)
            T3->setParent(granddaddy);

          granddaddy->setRight(T4);

          if(T4 != NULL)
            T4->setParent(granddaddy);

          daddy->setRight(T2);

          if(T2 != NULL)
            T2->setParent(daddy);

          daddy->setLeft(T1);

          if(T1 != NULL)
            T1->setParent(daddy);

          this->setValue(temp);

          this->setRight(granddaddy);

          this->setLeft(daddy);

          return (Splay<T> *)granddaddy->getParent();
        }
      }
    }
  }

}

template <class T>
Splay<T> * Splay<T>::splay_insert( const T & v ) {
  typedef Tree<T> Base;
  Splay<T> * current;

  if(Base::right != NULL) {
    if(Base::right->getValue() == v) {
      current = (Splay<T> *)Base::right;
    }
  }
  if(Base::left != NULL) {
    if(Base::left->getValue() == v) {
      current = (Splay<T> *)Base::left;
    }
  }

  return current->splay();

}

template <class T>
Splay<T> * Splay<T>::getRoot( ) {
  typedef Tree<T> Base;

  Splay<T> * temp;

  if(Base::parent == NULL) {
    return this;
  } else {
    temp = (Splay<T> *)Base::parent;
    temp->getRoot();
    return temp;
  }

}
