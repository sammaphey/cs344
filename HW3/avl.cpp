template <class T>
AVL<T> * AVL<T>::search( const T &v ) {
  typedef Tree<T> Base;

  if(v == Base::value) {
    return this;
  } else if (v < Base::value) {
    if(Base::left != NULL) {
      return (AVL<T> *)Base::left->search(v);
    } else {
      return NULL;
    }
  } else {
    if(Base::right != NULL) {
      return (AVL<T> *)Base::right->search(v);
    } else {
      return NULL;
    }
  }

}

template <class T>
AVL<T> * AVL<T>::insert( const T &v ) {
  typedef Tree<T> Base;

  int diff, diff_2;


  if(Base::value < v) {
    // If the value [v] we are currently trying to insert is greater than the current node [Base]
    //  meaning that we are going to insert on the right side of the current node.

    if(Base::right == NULL) {
      // If the right side is equal to NULL (meaning there is no leaf) add the node here

      Base::right = new AVL<T>(v, 0, 0, this);
    } else {
      //Otherwise recursively insert on the right side of the tree

      Base::right = Base::right->insert(v);
    }
  } else {
    // If the value [v] we are currently trying to insert is less than the current node [Base]
    //  meaning that we are going to insert on the left side of the current node.

    if(Base::left == NULL) {
      // If the left side is equal to NULL (meaning there is no leaf) add the node here

      Base::left = new AVL<T>(v, 0, 0, this);
    } else {
      //Otherwise recursively insert on the left side of the tree

      Base::left = Base::left->insert(v);
    }
  }

  Base::updateHeight();

  return balance( );

}

template <class T>
AVL<T> * AVL<T>::remove( const T &v ) {
  typedef Tree<T> Base;

  AVL<T> * temp;

  if(v == Base::value) {

    if(Base::left == NULL && Base::right == NULL) {
      delete this;
    } else if(Base::left == NULL && Base::right != NULL ) {
      temp = (AVL<T> *)Base::right;
      temp->setParent(Base::parent);
      delete this;
      return temp;
    } else if(Base::left != NULL && Base::right == NULL) {
      temp = (AVL<T> *)Base::left;
      temp->setParent(Base::parent);
      delete this;
      return temp;
    } else {
      temp = (AVL<T> *)Base::successor();
      T new_temp = temp->value;

      Base::right = (AVL<T> *)Base::right->remove(new_temp);
      Base::value = new_temp;

      return balance();
    }

  } else if(v > Base::value) {
    Base::right = Base::right->remove(v);
    return balance();
  } else {
    Base::left = Base::left->remove(v);
    return balance();
  }

  return NULL;

}

template <class T>
AVL<T> * AVL<T>::balance() {
  typedef Tree<T> Base;

  int diff, diff_2;

  heightDifference(diff, diff_2);

  // if the difference is between -1 and 1 (meaning the tree is balanced) do nothing
  if(diff <= 1 && diff >= -1) {
    return this;
  } else {
    if(diff < -1) {
      // If the difference is -1 then we know this must be a right-heavy unbalanced tree

      if(diff_2 >= 0) {
        // If the difference between the left and right child of the right child of the root is greater than 0

        AVL<T> * before_z = this;
        AVL<T> * before_y = (AVL<T> *)Base::right;
        AVL<T> * before_x = (AVL<T> *)before_y->getLeft();

        AVL<T> * after_z = new AVL<T>(before_z);
        AVL<T> * after_x = new AVL<T>(before_x);
        AVL<T> * after_y = new AVL<T>(before_y);

        after_y->setLeft(before_x->getRight());
        after_z->setRight(before_x->getLeft());
        after_x->setLeft(after_z);
        after_x->setRight(after_y);

        return after_x;
      } else if(diff_2 < 0) {
        AVL<T> * before_z = this;
        AVL<T> * before_y = (AVL<T> *)Base::right;
        AVL<T> * before_x = (AVL<T> *)before_y->getRight();

        AVL<T> * after_z = new AVL<T>(before_z);
        AVL<T> * after_x = new AVL<T>(before_x);
        AVL<T> * after_y = new AVL<T>(before_y);

        after_z->setRight(before_y->getLeft());
        after_y->setRight(after_x);
        after_y->setLeft(after_z);

        return after_y;
      } else {

        return this;
      }
    } else if(diff > 1) {
      if(diff_2 < 0) {
        AVL<T> * before_z = this;
        AVL<T> * before_y = (AVL<T> *)Base::left;
        AVL<T> * before_x = (AVL<T> *)before_y->getRight();

        AVL<T> * after_z = new AVL<T>(before_z);
        AVL<T> * after_x = new AVL<T>(before_x);
        AVL<T> * after_y = new AVL<T>(before_y);

        after_y->setRight(before_x->getLeft());
        after_z->setLeft(before_x->getRight());
        after_x->setLeft(after_y);
        after_x->setRight(after_z);

        return after_x;
      } else if(diff_2 >= 0) {
        AVL<T> * before_z = this;
        AVL<T> * before_y = (AVL<T> *)Base::left;
        AVL<T> * before_x = (AVL<T> *)before_y->getLeft();

        AVL<T> * after_z = new AVL<T>(before_z);
        AVL<T> * after_x = new AVL<T>(before_x);
        AVL<T> * after_y = new AVL<T>(before_y);

        after_z->setLeft(before_y->getRight());
        after_y->setLeft(after_x);
        after_y->setRight(after_z);
        return after_y;
      } else {
        return this;
      }
    } else {
      return this;
    }
  }

}

// template <class T>
// void AVL<T>::updateHeight( ) {
// }

template <class T>
void AVL<T>::heightDifference( int& diff, int& diff_2 ) {
  typedef Tree<T> Base;

  AVL<T> * left;
  AVL<T> * right;
  AVL<T> * right_right;
  AVL<T> * left_left;
  AVL<T> * left_right;
  AVL<T> * right_left;

  int right_height, left_height, left_left_height, left_right_height,
      right_right_height, right_left_height;


  if(Base::left != NULL) {
    // If the left side of the current node is not NULL (meaning there is a node there)

    left = (AVL<T> *)Base::left;

    //get the height of the left child of the current node
    left_height = left->getHeight();

    //get the height of the left child of the left child of the current node
    left_left = (AVL<T> *)left->getLeft();

    //get the height of the right child of the left child of the current node
    left_right = (AVL<T> *)left->getRight();

    if(left_left != NULL) {
      //if the left child of the left child of the current node is not NULL
      // (meaning there is a node there) calulate the height of the node

      left_left_height = left_left->getHeight();
    } else {
      // Otherwise set the value of the height equal to -1

      left_left_height = -1;
    }

    if(left_right != NULL) {
      //if the right child of the left child of the current node is not NULL
      // (meaning there is a node there) calulate the height of the node

      left_right_height = left_right->getHeight();
    } else {
      // Otherwise set the value of the height equal to -1

      left_right_height = -1;
    }

    // calculate the differences between the two
    diff_2 = left_left_height - left_right_height;

  } else {
    // otherwise (there is no left node) set the height to be -1

    left_height = -1;
  }

  // <-----
  // This section follows the same direction from above
  if(Base::right != NULL) {
    right = (AVL<T> *)Base::right;
    right_height = right->getHeight();
    right_right = (AVL<T> *)right->getRight();
    right_left = (AVL<T> *)right->getLeft();

    if(right_right != NULL) {
      right_right_height = right_right->getHeight();
    } else {
      right_right_height = -1;
    }

    if(right_left != NULL) {
      right_left_height = right_left->getHeight();
    } else {
      right_left_height = -1;
    }

    diff_2 = right_left_height - right_right_height;
  } else {
    right_height = -1;
  }
  // ----->

  // calculate the difference in heights between the left and right
  diff = left_height - right_height;

}
