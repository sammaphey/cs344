template <class T>

BST<T> * BST<T>::search( const T &v ) {
  typedef Tree<T> Base;

  if(v == Base::value) {
    return this;
  } else if (v < Base::value) {
    if(Base::left != NULL) {
      return (BST<T> *)Base::left->search(v);
    } else {
      return NULL;
    }
  } else {
    if(Base::right != NULL) {
      return (BST<T> *)Base::right->search(v);
    } else {
      return NULL;
    }
  }
}

template <class T>
BST<T> * BST<T>::insert( const T &v ) {
  typedef Tree<T> Base;

  if(Base::value < v) {
    if(Base::right == NULL) {
      Base::right = new BST<T>(v, 0, 0, this);
    } else {
      Base::right = Base::right->insert(v);
    }
  } else {
    if(Base::left == NULL) {
      Base::left = new BST<T>(v, 0, 0, this);
    } else {
      Base::left = Base::left->insert(v);
    }
  }

  return this;
}

template <class T>
BST<T> * BST<T>::remove( const T &v ) {
  typedef Tree<T> Base;

  BST<T> * temp;

  if(v == Base::value) {

    if(Base::left == NULL && Base::right == NULL) {
      delete this;
    } else if(Base::left == NULL && Base::right != NULL ) {
      temp = (BST<T> *)Base::right;
      temp->setParent(Base::parent);
      delete this;
      return temp;
    } else if(Base::left != NULL && Base::right == NULL) {
      temp = (BST<T> *)Base::left;
      temp->setParent(Base::parent);
      delete this;
      return temp;
    } else {
      temp = (BST<T> *)Base::successor();
      T new_temp = temp->value;

      Base::right = (BST<T> *)Base::right->remove(new_temp);
      Base::value = new_temp;

      return this;
    }

  } else if(v > Base::value) {
    Base::right = Base::right->remove(v);
    return this;
  } else {
    Base::left = Base::left->remove(v);
    return this;
  }

  return NULL;

}
